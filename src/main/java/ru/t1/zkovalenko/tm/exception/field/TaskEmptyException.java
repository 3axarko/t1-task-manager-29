package ru.t1.zkovalenko.tm.exception.field;

public final class TaskEmptyException extends AbstractFieldException {

    public TaskEmptyException() {
        super("Task is empty");
    }

}
