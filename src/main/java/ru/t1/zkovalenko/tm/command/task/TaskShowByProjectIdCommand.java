package ru.t1.zkovalenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.exception.entity.TaskNotFoundException;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task by project id";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        if (tasks.size() == 0) throw new TaskNotFoundException();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
